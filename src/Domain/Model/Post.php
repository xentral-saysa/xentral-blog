<?php

declare(strict_types=1);

namespace App\Domain\Model;

class Post
{
    private int $id;
    private string $slug;
    private string $author;
    private \DateTime $created_at;
    private ?\DateTime $updated_at = null;
    private string $title;
    private string $excerpt;
    private string $content;

    public function __construct(
        string $slug,
        string $author,
        string $title,
        string $excerpt,
        string $content
    ) {
        $this->slug = $slug;
        $this->author = $author;
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->content = $content;
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updated_at;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getExcerpt(): string
    {
        return $this->excerpt;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
