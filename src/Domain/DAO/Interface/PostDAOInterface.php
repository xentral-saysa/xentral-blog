<?php

declare(strict_types=1);

namespace App\Domain\DAO\Interface;

use App\Domain\Model\Post;

interface PostDAOInterface
{
    public function getPosts(): array;
    public function getPostBySlug(string $slug): bool|Post;
    public function build(array $row): Post;
}
