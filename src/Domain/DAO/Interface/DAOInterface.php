<?php

declare(strict_types=1);

namespace App\Domain\DAO\Interface;

interface DAOInterface
{
    public function getDatabase();
    public function query(string $sql, $parameters = null);
}
