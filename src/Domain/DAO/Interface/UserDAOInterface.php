<?php

declare(strict_types=1);

namespace App\Domain\DAO\Interface;

interface UserDAOInterface
{
    public function create(string $username, string $password);
}
