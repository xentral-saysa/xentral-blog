<?php

declare(strict_types=1);

namespace App\Domain\DAO;

use App\Domain\DAO\Interface\PostDAOInterface;
use App\Domain\Model\Post;

class PostDAO extends DAO implements PostDAOInterface
{
    public function getPosts(): array
    {
        $sql = "SELECT * FROM post ORDER BY id DESC";
        $result = $this->query($sql);
        $posts = [];
        foreach ($result as $row) {
            $posts[$row['id']] = $this->build($row);
        }

        return $posts;
    }

    public function getPostBySlug(string $slug): bool|Post
    {
        $sql = "SELECT * FROM post WHERE slug = ?";
        $result = $this->query($sql, [$slug]);
        $row = $result->fetch();
        if ($row) {
            return $this->build($row);
        }

        return false;
    }

    public function build(array $row): Post
    {
        return new Post(
            $row['slug'],
            $row['author'],
            $row['title'],
            $row['excerpt'],
            $row['content']
        );
    }
}
