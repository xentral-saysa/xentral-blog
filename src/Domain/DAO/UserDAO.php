<?php

declare(strict_types=1);

namespace App\Domain\DAO;

use App\Domain\DAO\Interface\UserDAOInterface;

class UserDAO extends DAO implements UserDAOInterface
{
    public function create(string $username, string $password)
    {
        $this->query("INSERT INTO user (username, password) VALUES (?, ?)", [$username, $password]);
    }
}
