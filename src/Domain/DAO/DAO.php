<?php

declare(strict_types=1);

namespace App\Domain\DAO;

use App\Domain\DAO\Interface\DAOInterface;

abstract class DAO implements DAOInterface
{
    private ?\PDO $pdo = null;

    public function getDatabase()
    {
        if ($this->pdo === null) {
            return $this->dbConnect();
        }

        return $this->pdo;
    }

    private function dbConnect()
    {
        try {
            $this->pdo = new \PDO(DB_HOST, DB_USER, DB_PASSWORD);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $this->pdo;
        } catch (\Exception $e) {
            die('Database connexion error: ' . $e->getMessage());
        }
    }

    public function query(string $sql, $parameters = null)
    {
        if ($parameters) {
            $result = $this->getDatabase()->prepare($sql);
            $result->execute($parameters);

            return $result;
        } else {
            return $this->getDatabase()->query($sql);
        }
    }
}
