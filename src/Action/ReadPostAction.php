<?php


declare(strict_types=1);

namespace App\Action;

use App\Action\Interface\ReadPostActionInterface;
use App\Domain\DAO\Interface\PostDAOInterface;
use App\Responder\ReadPostActionResponder;

final class ReadPostAction implements ReadPostActionInterface
{
    private ReadPostActionResponder $responder;
    private PostDAOInterface $postDAO;

    /**
     * ReadPostAction constructor.
     *
     * @param  ReadPostActionResponder  $responder
     * @param  PostDAOInterface  $postDAO
     */
    public function __construct(ReadPostActionResponder $responder, PostDAOInterface $postDAO)
    {
        $this->responder = $responder;
        $this->postDAO   = $postDAO;
    }

    public function __invoke($slug, $id)
    {
        if ($post = $this->postDAO->getPostBySlug($slug)) {
            $responder = $this->responder;
            return $responder($post);
        } else {
            throw new \Exception('Post not found');
        }
    }
}
