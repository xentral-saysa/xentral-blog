<?php

declare(strict_types=1);

namespace App\Action\Interface;

interface HomepageActionInterface
{
    public function __invoke();
}
