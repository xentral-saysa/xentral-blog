<?php

declare(strict_types=1);

namespace App\Action\Interface;

interface ReadPostActionInterface
{
    public function __invoke($slug, $id);
}
