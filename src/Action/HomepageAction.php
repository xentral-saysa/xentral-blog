<?php

declare(strict_types=1);

namespace App\Action;

use App\Action\Interface\HomepageActionInterface;
use App\Domain\DAO\Interface\PostDAOInterface;
use App\Responder\HomePageActionResponder;

final class HomepageAction implements HomepageActionInterface
{
    private HomePageActionResponder $responder;
    private PostDAOInterface $postDAO;

    /**
     * HomepageAction constructor.
     *
     * @param  HomePageActionResponder  $responder
     * @param  PostDAOInterface  $postDAO
     */
    public function __construct(HomePageActionResponder $responder, PostDAOInterface $postDAO)
    {
        $this->responder = $responder;
        $this->postDAO   = $postDAO;
    }

    public function __invoke()
    {
        $posts = $this->postDAO->getPosts();
        $responder = $this->responder;
        return $responder($posts);
    }
}
