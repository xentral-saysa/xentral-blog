<?php

declare(strict_types=1);

namespace App\Responder;

use App\Domain\Model\Post;
use App\Responder\Interface\ReadPostActionResponderInterface;
use Core\Interface\ResponseInterface;
use Twig\Environment;

final class ReadPostActionResponder implements ReadPostActionResponderInterface
{
    private Environment $templating;
    private ResponseInterface $response;

    /**
     * ReadPostActionResponder constructor.
     *
     * @param  Environment  $templating
     * @param  ResponseInterface  $response
     */
    public function __construct(Environment $templating, ResponseInterface $response)
    {
        $this->templating = $templating;
        $this->response   = $response;
    }

    public function __invoke(Post $post)
    {
        $body = $this->templating->render('post.twig.html', [
            'post' => $post
        ]);

        $this->response->withBody($body)->send();
    }
}
