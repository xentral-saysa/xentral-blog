<?php

declare(strict_types=1);

namespace App\Responder;

use App\Responder\Interface\HomePageActionResponderInterface;
use Core\Interface\ResponseInterface;
use Twig\Environment;

final class HomePageActionResponder implements HomePageActionResponderInterface
{
    private Environment $templating;
    private ResponseInterface $response;

    /**
     * HomePageActionResponder constructor.
     *
     * @param  Environment  $templating
     * @param  ResponseInterface  $response
     */
    public function __construct(Environment $templating, ResponseInterface $response)
    {
        $this->templating = $templating;
        $this->response   = $response;
    }

    public function __invoke($posts)
    {
        $body = $this->templating->render('homepage.twig.html', [
            'posts' => $posts,
        ]);

        $this->response->withBody($body)->send();
    }
}
