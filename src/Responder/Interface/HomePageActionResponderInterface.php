<?php

declare(strict_types=1);

namespace App\Responder\Interface;

interface HomePageActionResponderInterface
{
    public function __invoke(array $posts);
}
