<?php

declare(strict_types=1);

namespace App\Responder\Interface;

use App\Domain\Model\Post;

interface ReadPostActionResponderInterface
{
    public function __invoke(Post $post);
}
