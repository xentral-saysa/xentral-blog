<?php

declare(strict_types=1);

namespace Config;

/**
 * Contains all the routes for the application
 */
final class Routes
{
    public static $routes = [

        '#^/?$#' => [
            'action' => 'App\Action\HomepageAction',
            'params_keys' => null,
        ],
        '#^/posts/([a-z-]+)/([0-9]+)$#i' => [
            'action' => 'App\Action\ReadPostAction',
            'params_keys' => [
                'slug',
                'id',
            ],
        ],
        #'#^/admin/posts/add$#i' => 'addPost',
        #'#^/admin/posts/edit/([0-9]+)$#i' => 'editPost',
        #'#^/admin$#i' => 'admin',
    ];
}
