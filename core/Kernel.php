<?php

declare(strict_types=1);

namespace Core;

use App\Action\HomepageAction;
use App\Action\ReadPostAction;
use App\Domain\DAO\PostDAO;
use App\Responder\HomePageActionResponder;
use App\Responder\ReadPostActionResponder;
use Config\Routes;
use Core\Interface\ContainerInterface;
use Core\Interface\KernelInterface;
use Core\Interface\RequestInterface;
use Core\Interface\RouterInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * The application kernel
 *
 * @version 0.0.1
 * @author Saysa Bounkhong <saysabou@gmail.com>
 */
final class Kernel implements KernelInterface
{
    private ContainerInterface $container;
    private RequestInterface $request;
    private RouterInterface $router;

    public function init()
    {
        $this->container = new Container();
        $this->define();
        $this->registerMiddlewareDependencies();
        $this->registerApplicationDependencies();
        $this->registerApplicationRoutes();

        return $this;
    }

    public function run()
    {
        $this->request = $this->container->get('request');

        try {
            $this->router->resolve($this->request->getUriPath());
            $this->router->dispatch();
        } catch (\Exception $e) {

            // todo log it
            echo $e->getMessage();
        }
    }

    # define constants.
    private function define()
    {
        include_once(dirname(dirname(__FILE__)) . '/config/constants.php');
    }

    private function registerMiddlewareDependencies()
    {
        $container = $this->container;
        $container->set('request', function () {
            return new Request(
                $_SERVER['SERVER_PROTOCOL'],
                $_SERVER['REQUEST_METHOD'],
                getallheaders(),
                $_SERVER['REQUEST_URI']
            );
        });

        $container->set('response', function () {
            return new Response();
        });

        $container->set('router', function () use ($container) {
            return new Router($container);
        });

        $container->set('twig', function () {
            $loader = new FilesystemLoader(TEMPLATE_DIR);
            return new Environment($loader);
        });
    }

    private function registerApplicationRoutes()
    {
        $this->router = $this->container->get('router');

        foreach (Routes::$routes as $pattern => $endpoint) {
            $this->router->addRoute(new Route($pattern, $endpoint['action'], $endpoint['params_keys']));
        }
    }

    private function registerApplicationDependencies()
    {
        $container = $this->container;

        $container->set('App\Domain\DAO\PostDAO', function () {
            return new PostDAO();
        });

        $container->set('App\Responder\HomePageActionResponder', function () use ($container) {
            return new HomePageActionResponder(
                $container->get('twig'),
                $container->get('response')
            );
        });
        $container->set('App\Responder\ReadPostActionResponder', function () use ($container) {
            return new ReadPostActionResponder(
                $container->get('twig'),
                $container->get('response')
            );
        });
        $container->set('App\Action\HomepageAction', function () use ($container) {
            return new HomepageAction(
                $container->get('App\Responder\HomePageActionResponder'),
                $container->get('App\Domain\DAO\PostDAO')
            );
        });
        $container->set('App\Action\ReadPostAction', function () use ($container) {
            return new ReadPostAction(
                $container->get('App\Responder\ReadPostActionResponder'),
                $container->get('App\Domain\DAO\PostDAO')
            );
        });
    }
}
