<?php

declare(strict_types=1);

namespace Core;

use Core\Interface\RouteInterface;

class Route implements RouteInterface
{
    private string $pattern;
    private string $action;
    private ?array $params_keys;

    public function __construct(string $pattern, string $action, ?array $params_keys)
    {
        $this->pattern = $pattern;
        $this->action = $action;
        $this->params_keys = $params_keys;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return ?array
     */
    public function getParamsKeys(): ?array
    {
        return $this->params_keys;
    }
}
