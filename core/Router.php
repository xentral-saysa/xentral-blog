<?php

declare(strict_types=1);

namespace Core;

use App\Exceptions\NotFoundException;
use Core\Interface\ContainerInterface;
use Core\Interface\RouteInterface;
use Core\Interface\RouterInterface;

final class Router implements RouterInterface
{
    private $routes = [];
    private ?string $action = null;
    private $params = [];
    private ContainerInterface $container;

    /**
     * Router constructor.
     *
     * @param  ContainerInterface  $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function addRoute(RouteInterface $route)
    {
        $this->routes[] = $route;
    }

    /**
     * Resolves the given URI's path based on the registered routes.
     *
     * @param  string  $uriPath
     */
    public function resolve(string $uriPath)
    {
        $params_uri = [];
        foreach ($this->routes as $route) {
            if (preg_match($route->getPattern(), $uriPath, $matches)) {
                $this->action = $route->getAction();

                if (sizeof($matches)>1) {
                    foreach ($matches as $key => $param) {
                        if ($key > 0) {
                            $params_uri[] = $param;
                        }
                    }
                    foreach ($params_uri as $key => $param) {
                        $this->params[$route->getParamsKeys()[$key]] = $param;
                    }
                }
                break;
            }
        }

        if (is_null($this->action)) {
            throw new NotFoundException('Route not Found');
        }
    }

    public function dispatch()
    {
        $action = $this->container->get($this->action);
        call_user_func_array($action, $this->params);
    }
}
