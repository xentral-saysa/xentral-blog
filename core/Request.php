<?php

declare(strict_types=1);

namespace Core;

use Core\Interface\RequestInterface;

class Request implements RequestInterface
{
    private string $protocolVersion;
    private string $method;
    private array $headers;
    private string $uriPath;

    /**
     * Request constructor.
     *
     * @param  string  $protocolVersion
     * @param  string  $method
     * @param  array  $headers
     * @param  string  $uriPath
     */
    public function __construct(string $protocolVersion, string $method, array $headers, string $uriPath)
    {
        $this->protocolVersion = $protocolVersion;
        $this->method          = $method;
        $this->headers         = $headers;
        $this->uriPath         = $uriPath;
    }

    /**
     * @return string
     */
    public function getProtocolVersion(): string
    {
        return $this->protocolVersion;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getUriPath(): string
    {
        return $this->uriPath;
    }
}
