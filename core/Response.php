<?php

declare(strict_types=1);

namespace Core;

use Core\Interface\ResponseInterface;

class Response implements ResponseInterface
{
    protected $body;
    protected $status_code;

    public function withBody(string $body)
    {
        $this->body = $body;

        return $this;
    }

    public function send($status_code = 200)
    {
        http_response_code($status_code);
        echo $this->body;
    }
}
