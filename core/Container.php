<?php

declare(strict_types=1);

namespace Core;

use Core\Interface\ContainerInterface;

final class Container implements ContainerInterface
{
    /**
     * @var array
     */
    private $registry = [];

    /**
     * @param  string  $id
     * @param  callable  $callable
     *
     * @return $this
     */
    public function set(string $id, callable $callable): Container
    {
        $this->registry[$id] = $callable;

        return $this;
    }

    /**
     * @param  string  $id
     *
     * @return mixed
     * @throws \Exception
     */
    public function get(string $id)
    {
        if (! $this->has($id)) {
            throw new \Exception('No entry for ' . $id);
        } elseif (! is_callable($this->registry[$id])) {
            throw new \Exception('Error while retrieving the entry');
        } else {
            return $this->registry[$id]();
        }
    }

    /**
     * @param  string  $id
     *
     * @return bool
     */
    public function has(string $id)
    {
        return isset($this->registry[$id]);
    }
}
