<?php

declare(strict_types=1);

namespace Core\Interface;

interface RouterInterface
{
    public function addRoute(RouteInterface $route);
    public function resolve(string $uriPath);
    public function dispatch();
}
