<?php

declare(strict_types=1);

namespace Core\Interface;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    public function set(string $id, callable $callable);
}
