<?php

declare(strict_types=1);

namespace Core\Interface;

interface RequestInterface
{
    public function getUriPath(): string;
}
