<?php

declare(strict_types=1);

namespace Core\Interface;

interface KernelInterface
{
    public function init();
    public function run();
}
