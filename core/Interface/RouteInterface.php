<?php

declare(strict_types=1);

namespace Core\Interface;

interface RouteInterface
{
    public function getPattern();
    public function getAction();
}
