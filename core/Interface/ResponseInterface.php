<?php

declare(strict_types=1);

namespace Core\Interface;

interface ResponseInterface
{
    public function withBody(string $body);
    public function send();
}
