<?php
declare(strict_types=1);

namespace Tests\Domain\Model;

use App\Domain\Model\Post;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    /**
     * @covers \App\Domain\Model\Post
     */
    public function testTitle()
    {
        $post = new Post(
            'hello-world',
            'John Doe',
            'Hello World',
            'The excerpt',
            'The content'
        );

        $this->assertEquals('Hello World', $post->getTitle());
    }
}