<?php
declare(strict_types=1);

namespace Tests\Responder;

use App\Responder\HomePageActionResponder;
use Core\Interface\ResponseInterface;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

class HomePageActionResponderTest extends TestCase
{
    /**
     * @covers \App\Responder\HomePageActionResponder
     */
    public function testAction()
    {
        $twig = $this->createMock(Environment::class);
        $twig
            ->expects($this->once())
            ->method('render');
        $response = $this->createMock(ResponseInterface::class);
        $response
            ->expects($this->once())
            ->method('withBody')
            ->willReturn($response);
        $response
            ->expects($this->once())
            ->method('send');

        $responder = new HomePageActionResponder(
            $twig,
            $response
        );

        $this->assertInstanceOf(HomePageActionResponder::class, $responder);

        $responder([]);
    }
}