#!/bin/bash

# MySQL environment setup...
echo "Running MySQL environment setup for your application..."
echo "Creating the new database post table..."
docker exec xentral_mysql mysql -u user -ppassword_dev -e "USE dbxentral;
	CREATE TABLE IF NOT EXISTS post
	(
		id bigint unsigned NOT NULL AUTO_INCREMENT,
		slug varchar(254) NOT NULL,
		author varchar(50) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime,
		title varchar(255),
		excerpt tinytext,
		content longtext,
		CONSTRAINT pk_post_id PRIMARY KEY (id)
	);
	CREATE UNIQUE INDEX IF NOT EXISTS ix_post_slug ON post (slug);

	CREATE TABLE IF NOT EXISTS user
	(
		id bigint unsigned NOT NULL AUTO_INCREMENT,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime,
		username varchar(20),
		password varchar(255),
		CONSTRAINT pk_user_id PRIMARY KEY (id)
	);
	CREATE UNIQUE INDEX IF NOT EXISTS ix_user_username ON user (username);"
echo ""
echo "Installation done..."
exit