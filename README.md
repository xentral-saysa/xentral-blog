Xentral Blog
========

In order to use this project locally, please be sure to have Make installed.

## Usage
In order to use this project, you should have Docker & Docker-compose installed.

Once you've installed Docker, time to build the project.

**_In order to perform better, Docker can block your dependencies installation and return an error
or never change your php configuration, it is recommended to delete all your images/containers
before building the project_**

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -a -q) -f
```

To start the project:
- run `make build`
- run `make up`
- run `make setup-database`
- launch [http://localhost:8888](http://localhost:8888)

To enter into the container:
- run `make bash`

To create a user:
```bash
make bash
php bin/create_user
```

To run the test:
- run `make phpunit`

To stop the project:
- run `make stop`

## Contribution rules and process

1. Always create a new branch before developing a new feature or fixing errors
1. Run the tests first, in order to be sure you are starting with clean base  
1. The branch name should describe the issue
1. Build the feature and the associated tests
1. Before pushing your branch you must run `make code-quality`
1. Push your branch when all the tests are green

## To Xentral team

PHP 8 is used because there is no requirements regarding PHP and it's always better to use a latest version.

Ngnix as a web server because it is easier to configure against Apache.

The ADR is used because it is the best pattern that fits the HTTP protocole.

A DIC is used because it is easier to handle the dependencies.

What could be added:
- e2e tests
- CI script
- improve the installation process, with a script that take user inputs in order to create
  the application configuration.
- logger
- lots of refactoring
- forms with validators
- rich models
- cache system
- much more...
