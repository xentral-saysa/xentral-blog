bash:
	docker exec -it xentral_php-fpm bash
.PHONY: bash

build:
	docker-compose build --no-cache
.PHONY: build

code-quality:
	make cs-fixer
	make php-stan
.PHONY: code-quality

cs-fixer: vendor/bin/php-cs-fixer
	docker exec xentral_php-fpm vendor/bin/php-cs-fixer fix src
	docker exec xentral_php-fpm vendor/bin/php-cs-fixer fix core
	docker exec xentral_php-fpm vendor/bin/php-cs-fixer fix config
.PHONY: cs-fixer

dump-autoload: composer.json
	docker exec xentral_php-fpm composer dump-autoload --optimize --classmap-authoritative
.PHONY: dump-autoload

php-stan: vendor/bin/phpstan
	docker exec xentral_php-fpm vendor/bin/phpstan analyse -c phpstan.neon
.PHONY: php-stan

phpunit:
	docker exec xentral_php-fpm vendor/bin/phpunit tests --coverage-html web/test-coverage
.PHONY: phpunit

up: composer.json
	docker-compose up -d --build --remove-orphans --force-recreate
	docker exec xentral_php-fpm composer install  -a -o
	docker exec xentral_php-fpm composer clear-cache
	docker exec xentral_php-fpm composer dump-autoload --optimize --classmap-authoritative
.PHONY: up

setup-database: installer.sh
	chmod u+x ./installer.sh
	./installer.sh
.PHONY: setup-database

stop:
	docker-compose stop
.PHONY: stop